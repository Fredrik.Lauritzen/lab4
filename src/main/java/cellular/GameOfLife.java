package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {


	IGrid currentGeneration;

	
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		int rows = nextGeneration.numRows();
		int cols = nextGeneration.numColumns();
		for (int i = 0; i<rows; i++) {
			for (int j = 0; j<cols; j++) {
				nextGeneration.set(i, j, getNextCell(i,j));
			}	
		}
		currentGeneration =  nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		int aliveNeighbors = neighbourCount(row,col,CellState.ALIVE);
	
		
		if (currentGeneration.get(row, col) == CellState.ALIVE) {
			if (aliveNeighbors < 2 ) {
				 return CellState.DEAD;
			}
			if (aliveNeighbors == 2 || aliveNeighbors == 3) {
			     return CellState.ALIVE;
			}
			else return CellState.DEAD;
		}
		else {
			if (aliveNeighbors == 3) {
				 return CellState.ALIVE;
			}
			else return CellState.DEAD;
		}
	}

	private int neighbourCount(int row, int col, CellState state) {
		int count = 0;
		int totalRow = currentGeneration.numRows();
		int totalCol = currentGeneration.numColumns();

		if (currentGeneration.get(row, col).equals(state)) {
		count--;
		}
		
		for (int i=-1; i<2; i++) {
			for (int j=-1; j<2; j++) {
				if (((row + i) <= totalRow) && (row + i) >= 0) {
					if (((col + j) <= totalCol) && (col + j) >=0) {
						if (currentGeneration.get(row+i, col+j).equals(state)){
							count++;
						}	
					}	
				}
			}
		}
		
		
		return count;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
