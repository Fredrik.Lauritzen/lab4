package datastructure;

import java.util.ArrayList;
import java.util.List;

import cellular.CellState;


public class CellGrid implements IGrid {
	
	int i;
	int j;
	int cols; //width
	int rows; //heigth
	List<List<CellState>> stateTable = new ArrayList<List<CellState>>();
	
	
	
	

    public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
    	this.cols = columns;
    	this.stateTable = new ArrayList<List<CellState>>(0);
    	
    	for (i = 0; i < rows; i++) {
			this.stateTable.add(new ArrayList<>(0));
			
    		for (j = 0; j < columns; j++) {
    			this.stateTable.get(i).add(initialState);

    
    		}
    	}
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    public void set(int row, int column, CellState element) {
    	
    	if (row < 0 || row > numRows() || column < 0 || column > numColumns()) { // > numColumns()?
    		throw new IndexOutOfBoundsException();
    		
    	}
    	else {
    		stateTable.get(row).set(column, element);
    		
    		
    	}
    }
  

    @Override
    public CellState get(int row, int column) {
    	if (row < 0 || row > numRows() || column < 0 || column > numColumns()) { // > numColumns()?
    		throw new IndexOutOfBoundsException();
        }
    	else {
    		return stateTable.get(row).get(column);
    	}
        
    }

    @Override
    public IGrid copy() {
    	CellGrid copyGrid = new CellGrid(rows, cols, CellState.DEAD);
    	
    	CellState copyState;
    	
    	for (int i = 0; i < rows; i++) {
    		for (int j = 0; j < cols; j++) {
    			copyState = this.get(i,j);
    			copyGrid.set(i, j, copyState);
    			
    			
    		}
    	}
    	return copyGrid;
    	
    	
    	
    }
    
}
